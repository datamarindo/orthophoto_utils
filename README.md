![Minimal R Version](https://img.shields.io/badge/R%3E%3D-3.2.0-blue.svg)

# Herramientas para planeación de vuelos, así como para proceso y estadísticas de ortofotos
_E. Díaz_ Pronatura Veracruz A.C.
## Para planeación de vuelos en zonas montañosas de México

Una problemática a la que nos enfrentamos al hacer vuelos en zonas montañosas es que por lo general desconocemos las altitudes reales del sitio a volar, lo cual no se refleja en aplicaciones para planeación de vuelos tales como Pix4d. Cuando una zona dentro del polígono a volar queda considerablemente más elevada que el sitio donde despegamos existe la posibilidad de que el mosaico no ensamble bien en las partes altas, ya que el dron puede estar volando a poca altura y la cuadrícula no alcanza el traslape necesario.

![Ráster numérico](Rplot05.png)

Para facilitar el reconocimiento en campo de los sitios a volar _asistido por la aplicación de vuelo que tiene un polígono del sitio de interés previamente cargado_, desarrollé este script que descarga los datos de elevación de INEGI de resolución 50 m a partir del polígono de interés y los mapea como un ráster numérico (como la función d.rast.num de GRASS) junto con el polígono.

Previamente a la versión final del código se compararon tres fuentes de datos de elevación de distinta resolución geoespacial: cartas Lidar de INEGI (5 m), SRTM de NASA (30 m) y DEM de INEGI (50 m), en términos de: 1) cobertura espacial, 2) programabilidad de la descarga y 3) resolución geoespacial.

![Comparacion modelos de elevación](comparacion_dems.png)
![Cobertura lidar](Rplot06.png)

Las cartas Lidar abarcan poco más del 39% de la superficie del país, mientras que los otros dos productos sí lo cubren a nivel nacional e insular; en cuanto a programabilidad, las versiones de SRTM de 30 m requieren credenciales de ingreso a través de esta página: http://dwtkns.com/srtm30m/; otras versiones de SRTM de menor resolución (90 m) sí son descargables programáticamente, los Lidar y DEM de INEGI son descargables a través de queries geoespaciales usando unas capas de límites 1:20000 y 1:50000 previamente disponibles en este repositorio. Finalmente, la resolución de las cartas Lidar era exagerada para el propósito de vuelo, mientras que las del DEM de INEGI fueron las que mejor se visualizaron como ráster numérico.



